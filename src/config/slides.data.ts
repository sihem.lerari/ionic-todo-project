export const SLIDES = [
    {
      title: "Bienvenue sur TodoList",
      description: "Créer un compte pour enregistrer vos tâches et y accéder n'importe où. C'est gratuit et pour toujours.",
      image: "assets/imgs/app-logo.png",
    },
    {
      title: "Organisez votre vie",
      description: "Organisez et hiérarchisez vos tâches et vos projets pour vivre mieux et stresser moins.",
      image: "assets/imgs/app-logo.png",
    },
    {
      title: "Collaborez efficacement",
      description: "Partagez vos listes avec vos amis et gardez votre travail d'équipe organisé.",
      image: "assets/imgs/app-logo.png",
    }
];
