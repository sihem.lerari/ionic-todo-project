import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

@Injectable()
export class ToastProvider {

  constructor(public toastCtrl: ToastController) { }

 public showToast(msg: string, duration: number, position: string) {
   let toast = this.toastCtrl.create({
     message: msg,
     duration: duration,
     position: position
   });
   toast.present(toast);
 }

 public showToastWithCloseButton(msg: string) {
   const toast = this.toastCtrl.create({
     message: msg,
     showCloseButton: true,
     closeButtonText: 'Ok'
   });
   toast.present();
 }


}
