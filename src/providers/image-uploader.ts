import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Injectable()
export class ImageUploaderProvider {

  cameraImage : String;

  constructor(private camera : Camera) {
  }

  selectImage() : Promise<any>{
    return new Promise(resolve =>
    {
       let cameraOptions : CameraOptions = {
           sourceType         : this.camera.PictureSourceType.PHOTOLIBRARY,
           destinationType    : this.camera.DestinationType.DATA_URL,
           quality            : 100,
           targetWidth        : 320,
           targetHeight       : 240,
           encodingType       : this.camera.EncodingType.JPEG,
           mediaType          : this.camera.MediaType.PICTURE,
           correctOrientation : true
       };
       this.camera.getPicture(cameraOptions)
       .then((data) =>
       {
          this.cameraImage 	= "data:image/jpeg;base64," + data;
          resolve(this.cameraImage);
       });
    });
  }

}
