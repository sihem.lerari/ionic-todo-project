import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import { GooglePlus } from '@ionic-native/google-plus';
//import { Facebook, FacebookLoginResponse} from '@ionic-native/facebook';
import { Platform } from 'ionic-angular';
import {Subject} from 'rxjs/Subject';
import * as firebase from 'firebase';

@Injectable()
export class AuthProvider {

  isLog: Subject<boolean>;

  constructor(
  public platform: Platform,
  public googleplus: GooglePlus,
  //private fb: Facebook,
  private afAuth: AngularFireAuth) {

    this.isLog = new Subject<boolean>();
    firebase.auth().onAuthStateChanged( user => {
      if (user){
        this.isLog.next(true);
        console.log("mon id est : "+this.afAuth.auth.currentUser.uid);
      } else {
        this.isLog.next(false);
      }
    });

  }

  loginWithGoogle() {
    if (this.platform.is('cordova')){
      this.googleplus.login({
      'webClientId': '558492062070-9ssisdv08mnecnifmhmbmf985d6n5e0b.apps.googleusercontent.com',
      'offline': true
      })
    .then(  res => {
        firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
         .then( success => {
            console.log("Firebase success: " + JSON.stringify(success));
           // this.navCtrl.setRoot(ListPage);
          })
          .catch( error => console.log("Firebase failure: " + JSON.stringify(error)));

        }).catch(err => console.error("Error: ", err));
      }
  else {
     return this.afAuth.auth.signInWithPopup (new firebase.auth.GoogleAuthProvider())
    }

  }

  loginWithFacebook() {
 /*  if (this.platform.is('cordova')){
     this.fb.login(
       ['public_profile', 'user_friends', 'email']
      ).then((res: FacebookLoginResponse) => { console.log('Logged into Facebook!', res);
      // Store To App User Data. this.nav.setRoot(HomePage);
     }) .catch(e => console.error('errur connexion facebook',e));
   }
   else { */
    return this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider()
   ).then (res=>{
     console.log('connexion facebook succes!');
   })
   //}
  }

  getAuthState(){
    return this.afAuth.authState;
  }

  logout() {
    this.afAuth.auth.signOut();
  }

}
