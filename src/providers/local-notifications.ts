import { Injectable } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications';


@Injectable()
export class LocalNotificationsProvider {

  constructor(private localNotifications: LocalNotifications) {
  }

  scheduleNotification(title: string, text: string) {
  this.localNotifications.schedule({
    title: title,
    text: text
  });
  }

}
