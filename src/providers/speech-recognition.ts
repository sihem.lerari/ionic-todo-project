import { Injectable } from '@angular/core';
import { SpeechRecognition } from '@ionic-native/speech-recognition';

@Injectable()
export class SpeechRecognitionProvider {

  options: Object;

  constructor(private speechRecognition: SpeechRecognition){
      this.options= {
        language: 'fr-FR',
        prompt: 'Je vous écoute !',
        showPopup: true
      }
  }

  public startSpeechRecognition(){
    return this.speechRecognition.startListening(this.options);
  }

  public getPermission() {
    this.speechRecognition.hasPermission()
      .then((hasPermission: boolean) => {
        if (!hasPermission) {
          // Request permissions
          this.speechRecognition.requestPermission()
            .then(
              () => console.log('Granted'),
              () => console.log('Denied')
            )
        }
      });
  }
}
