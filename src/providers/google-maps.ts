import { Injectable } from '@angular/core';
import { GoogleMap, GoogleMapsEvent, LatLng } from '@ionic-native/google-maps';


@Injectable()
export class GoogleMapsProvider {

  map: GoogleMap;
  userPosition: LatLng;
  markerPosition: LatLng;

  constructor() {

  }

  public loadMap(htmlElement: string, position: any) {
    this.map = new GoogleMap(htmlElement);

    this.map.one(GoogleMapsEvent.MAP_READY).then(
      () => {
        console.log('Map is ready!');
        this.addMarkerOnMap(position);
      }
    );
 }

public selectPosition(){
    console.log('in service', this.markerPosition);
    return this.markerPosition;
  }

private addMarkerOnMap(pos: any){
  this.markerPosition= new LatLng(pos.latitude,pos.longitude);

  this.map.moveCamera({
        target: this.markerPosition,
        zoom: 16,
        tilt: 30
  });
  this.map.addMarker({
      title: 'your todo task will occur here!',
      animation: 'DROP',
      draggable: true,
      position: this.markerPosition
  })
  .then(marker => {
      marker.on(GoogleMapsEvent.MARKER_DRAG_END)
            .subscribe(() => {
              this.markerPosition= marker.getPosition();
            });
      });
  }


    public loadMapWithAllMarkers(htmlElement: string, items: any[]) {
      this.map = new GoogleMap(htmlElement);

      this.map.one(GoogleMapsEvent.MAP_READY).then(
        () => {
          console.log('Map is ready!');
          for (let i of items){

            this.map.addMarker({
                title: 'your todo task will occur here!',
                animation: 'DROP',
                position: new LatLng(i.latitude,i.longitude)
            })
          }

          this.map.moveCamera({
                target: new LatLng(items[0].latitude, items[0].longitude),
                zoom: 16,
                tilt: 30
          });
        }
      );
   }

}
