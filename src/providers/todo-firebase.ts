import { Injectable } from '@angular/core';
import { TodoList , TodoItem } from "../interfaces/todo";
import { AuthProvider } from './auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import firebase from "firebase";

@Injectable()
export class TodoFirebaseProvider {

   public currentUser :string;
   private basePath: string = '/lists';

   constructor( private afDatabase : AngularFireDatabase,
                private authProvider :AuthProvider) {
     this.authProvider.getAuthState().subscribe( auth => {
       if(auth){
         this.currentUser = auth.uid;
         this.afDatabase.database.goOnline();
       }
       else {
         this.currentUser = null;
         this.afDatabase.database.goOffline();
       }
     });
   }

    /* Source : https://gist.github.com/gordonbrander/2230317*/
    private generateUuid() {
      return Math.random().toString(36).substr(2, 9);
    };

    private todoLists():AngularFireList<TodoList> {
        return this.afDatabase.list<TodoList>(`${this.basePath}/`,
          ref => ref.orderByChild('users/'+this.currentUser).equalTo(true));
    }

    private todoItems(listUuid:string): AngularFireList<TodoItem>{
        return this.afDatabase.list<TodoItem>(`${this.basePath}/${listUuid}/items`);
    }

    private setProprietaire(uidList){
      this.afDatabase.list(`${this.basePath}/`+uidList).set('proprietaire',this.currentUser);
    }

    private setUserList(uidList){
      this.afDatabase.list(`${this.basePath}/`+uidList+'/users').set(this.currentUser,true);
    }

    /*lists*/

    public getLists(){
      return this.todoLists().valueChanges();
    }

    public addList(addedList : TodoList) {
        addedList.uuid= this.generateUuid();
        this.todoLists().set(addedList.uuid, addedList);
        this.setUserList(addedList.uuid);
        this.setProprietaire(addedList.uuid);
    }

    public deleteList(deletedList : TodoList){
        this.todoLists().remove(deletedList.uuid);
    }

    public updateList(updatedList : TodoList){
      this.todoLists().update(updatedList.uuid, updatedList);
    }

    /*items*/
    public getItems(listUuid:string){
        return this.todoItems(listUuid).valueChanges();
    }

    public addItem(listUuid: string, newItem: TodoItem) {
      if(!newItem.image.startsWith("assets/imgs/")){
        this.uploadImageOfItem(newItem.image).then(res=>{
          newItem.image=res.downloadURL;
          let itemList = this.todoItems(listUuid);
          newItem.uuid = this.generateUuid();
          return itemList.set(newItem.uuid, newItem);
        });
      }
      else {
        let itemList = this.todoItems(listUuid);
        newItem.uuid = this.generateUuid();
        return itemList.set(newItem.uuid, newItem);
      }

    }

    public updateItem(listUuid: string, editedItem: TodoItem) {
      return this.todoItems(listUuid).update(editedItem.uuid, editedItem);
    }

    public deleteItem(listUuid: string, uuid: string) {
      return this.todoItems(listUuid).remove(uuid);
    }

    uploadImageOfItem(imageString) : Promise<any>{
      let image       : string  = 'item-' + new Date().getTime() + '.jpg',
          storageRef  : any,
          parseUpload : any;

      return new Promise((resolve, reject) =>
      {
         storageRef       = firebase.storage().ref('items/' + image);
         parseUpload      = storageRef.putString(imageString, 'data_url');

         parseUpload.on('state_changed', (_snapshot) =>
         {
            // We could log the progress here IF necessary
            console.log('snapshot progess ' + _snapshot);
         },
         (err) =>
         {
            reject(err);
         },
         (success) =>
         {
            resolve(parseUpload.snapshot);
         });
      });
    }


       public partageList(uidList){
          this.setUserList(uidList);
       }

}
