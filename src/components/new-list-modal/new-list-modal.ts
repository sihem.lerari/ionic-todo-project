import { Component } from '@angular/core';
import { Platform, ViewController } from 'ionic-angular';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { SpeechRecognitionProvider } from '../../providers/speech-recognition';



@Component({
  selector: 'new-list-modal',
  templateUrl: 'new-list-modal.html'
})
export class NewListModalComponent {

  nameOfList: string='';
  constructor(public viewCtrl: ViewController,
              public platform: Platform,
              private todoFbProvider: TodoFirebaseProvider,
              private speechRecogProvider :SpeechRecognitionProvider) {
      if (this.platform.is('cordova')){
          this.platform.ready().then(() =>
            this.speechRecogProvider.getPermission())
        }
  }

  startListening(){
    this.speechRecogProvider.startSpeechRecognition()
    .subscribe(matches => {
             this.nameOfList = matches[0];
        });
  }

  addTodoList() {
    if (this.nameOfList !== ''){
       this.todoFbProvider.addList({ name: this.nameOfList });
        this.dismiss();
      }
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
