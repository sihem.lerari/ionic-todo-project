import { NgModule } from '@angular/core';
import { QrCodeModalComponent } from './qr-code-modal/qr-code-modal';
import { NewListModalComponent } from './new-list-modal/new-list-modal';
import { EditListModalComponent } from './edit-list-modal/edit-list-modal';
import { MapModalComponent } from './map-modal/map-modal';
import { DesciptionItemModalComponent } from './desciption-item-modal/desciption-item-modal';


@NgModule({
	declarations: [
    QrCodeModalComponent,
    NewListModalComponent,
    EditListModalComponent,
    MapModalComponent,
    DesciptionItemModalComponent],
	imports: [],
	exports: [
		QrCodeModalComponent,
    NewListModalComponent,
    EditListModalComponent,
    MapModalComponent,
    DesciptionItemModalComponent]
})
export class ComponentsModule {}
