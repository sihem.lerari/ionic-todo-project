import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';



@Component({
  selector: 'qr-code-modal',
  templateUrl: 'qr-code-modal.html'
})
export class QrCodeModalComponent {

  qrCode= null;


  constructor(public navParams: NavParams,
              public viewCtrl: ViewController) {
    this.createQrCode();
  }

  private createQrCode(){
    this.qrCode= this.navParams.get('listId');
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }


}
