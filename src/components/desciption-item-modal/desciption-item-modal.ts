import { Component } from '@angular/core';
import { Platform, ViewController, NavParams} from 'ionic-angular';
import { SpeechRecognitionProvider } from '../../providers/speech-recognition';


@Component({
  selector: 'desciption-item-modal',
  templateUrl: 'desciption-item-modal.html'
})
export class DesciptionItemModalComponent {

  descriptionOfItem: string;
  constructor(public viewCtrl: ViewController,
              public navParams: NavParams,
              public platform: Platform,
              private speechRecogProvider :SpeechRecognitionProvider) {

      this.descriptionOfItem= this.navParams.get('desciption');
      
      if (this.platform.is('cordova')){
          this.platform.ready().then(() =>
            this.speechRecogProvider.getPermission())
        }
  }

  startListening(){
    this.speechRecogProvider.startSpeechRecognition()
    .subscribe(matches => {
             this.descriptionOfItem = matches[0];
        });
  }


  dismiss() {
    this.viewCtrl.dismiss(this.descriptionOfItem);
  }

}
