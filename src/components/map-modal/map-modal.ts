import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ViewController } from 'ionic-angular';
import { GoogleMapsProvider } from '../../providers/google-maps';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'map-modal',
  templateUrl: 'map-modal.html'
})
export class MapModalComponent {

  showBtn: boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              public platform: Platform,
              private geolocation: Geolocation,
              private googleMapsProvider: GoogleMapsProvider
            ) {

    platform.ready().then(() => {
        this.showBtn= this.navParams.get('showBtn');

        let position= this.navParams.get('position');
        // if position is defined
        if (position !== null )
          this.googleMapsProvider.loadMap('map', position);
        else  // else load map on user location
          this.geolocateUser();
    });
  }

  selectPosition(){
    let position = this.googleMapsProvider.selectPosition();
    // send position to previous page before pop()
    this.navCtrl.getPrevious().data.locationOfItem = position;
    this.navCtrl.pop();
  }

  private geolocateUser() {
    this.geolocation.getCurrentPosition().then((resp) => {
       this.googleMapsProvider.loadMap('map',resp.coords);
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

}
