import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { TodoList } from '../../interfaces/todo';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';


@Component({
  selector: 'edit-list-modal',
  templateUrl: 'edit-list-modal.html'
})
export class EditListModalComponent {

  editedList: TodoList;

  constructor(public navParams: NavParams,
              public viewCtrl: ViewController,
              private todoFbProvider: TodoFirebaseProvider) {
    this.editedList= this.navParams.get('list');
  }


  EditTodoList(){
      this.todoFbProvider.updateList(this.editedList);
      this.viewCtrl.dismiss();
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }


}
