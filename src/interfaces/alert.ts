export interface Alert {
   title: string;
   message: string;
   input? : AlertInput;
   button_1: AlertButton;
   button_2: AlertButton;
}


export interface AlertInput {
  type?: string;
  name: string | number;
  placeholder?: string;
  value?: string;
}
export interface AlertButton {
  text: string;
  role?: string;
  cssClass?: string;
  handler: (value: any) => boolean|void;
}
