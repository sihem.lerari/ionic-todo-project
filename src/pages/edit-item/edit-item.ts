import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { TodoItem } from '../../interfaces/todo';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { ImageUploaderProvider } from "../../providers/image-uploader";
import { MapModalComponent } from '../../components/map-modal/map-modal';
import { DesciptionItemModalComponent } from '../../components/desciption-item-modal/desciption-item-modal';


@IonicPage()
@Component({
  selector: 'page-edit-item',
  templateUrl: 'edit-item.html',
})
export class EditItemPage {


  editedItem: TodoItem;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              public viewCtrl: ViewController,
              private todoFbProvider: TodoFirebaseProvider,
              private imgUploaderProvider: ImageUploaderProvider) {
    this.editedItem= this.navParams.get('item');
  }

  editItem(){
    let listId= this.navParams.get('listId');
    this.todoFbProvider.updateItem(listId, this.editedItem);
    this.viewCtrl.dismiss();
  }

  selectImage(){
  this.imgUploaderProvider.selectImage().then(res=> this.editedItem.image=res );
  }

  locationItem(){
    this.navCtrl.push(MapModalComponent,  {position: {latitude : this.editedItem.latitude, longitude: this.editedItem.longitude}, showBtn:true});
  }
  public ionViewWillEnter() {
    // retirieve data from map
    let pos = this.navParams.get('locationOfItem')|| null;
      if (pos !== null ){
          this.editedItem.latitude= pos.lat;
          this.editedItem.longitude= pos.lng;
      }
  }

  descriptionOfItem(){
    let modal = this.modalCtrl.create(DesciptionItemModalComponent, {desciption: this.editedItem.desc});
    modal.onDidDismiss(desciption => {
     this.editedItem.desc= desciption
    });
    modal.present();

  }

}
