import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditItemPage } from './edit-item';
import { MapModalComponent } from '../../components/map-modal/map-modal';

@NgModule({
  declarations: [
    EditItemPage,
    MapModalComponent
  ],
  entryComponents: [
    EditItemPage,
    MapModalComponent ],
  imports: [
    IonicPageModule.forChild(EditItemPage),
  ],
})
export class EditItemPageModule {}
