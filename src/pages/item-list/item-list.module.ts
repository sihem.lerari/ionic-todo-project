import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemListPage } from './item-list';
import { ItemPageModule } from '../item/item.module';
import { NewItemPageModule } from '../new-item/new-item.module';


@NgModule({
  declarations: [
    ItemListPage,
  ],
  entryComponents: [
     ],
  imports: [
    ItemPageModule,
    NewItemPageModule,
    IonicPageModule.forChild(ItemListPage),
  ],
})
export class ItemListPageModule {}
