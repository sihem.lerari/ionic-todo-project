import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { NewItemPage } from '../new-item/new-item';
import { ItemPage } from '../item/item';
import { LocalNotificationsProvider } from '../../providers/local-notifications';


@IonicPage()
@Component({
  selector: 'page-item-list',
  templateUrl: 'item-list.html',
})
export class ItemListPage {

  todoItems: any[] = [];
  listUuid: string;

  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              public navParams: NavParams,
              private todoFbProvider: TodoFirebaseProvider,
              private notif: LocalNotificationsProvider) {

    this.listUuid =this.navParams.get('listId');
    this.todoFbProvider.getItems(this.listUuid).subscribe( result =>{
       this.todoItems = result;
       this.notif.scheduleNotification('Item modifié','un item a été modifié');
    }
      );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ItemListPage');
  }

  addTodoItem(){
    this.navCtrl.push(NewItemPage,{listId: this.listUuid });
  }

  goToItemDetails(item){
    this.navCtrl.push(ItemPage,{ item: item, listId: this.listUuid });
  }

}
