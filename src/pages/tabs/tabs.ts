import { Component } from '@angular/core';

import { MapPage } from '../map/map';
import { ProfilPage } from '../profil/profil';
import { ListPage } from '../list/list';

import { AdMobFree, AdMobFreeBannerConfig, AdMobFreeInterstitialConfig } from '@ionic-native/admob-free';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ListPage;
  tab2Root = MapPage;
  tab3Root = ProfilPage;

  constructor( public admob: AdMobFree) {
    this.showBanner();
   // this.launchInterstitial(); // desactiver pour pouvoir tester sur web!!!!
  }

 

  showBanner() {
 
    let bannerConfig: AdMobFreeBannerConfig = {
        isTesting: true, 
        autoShow: true
      
    };

    this.admob.banner.config(bannerConfig);

    this.admob.banner.prepare().then(() => {
        // success
    }).catch(e => console.log(e));

}

launchInterstitial() {
 
  let interstitialConfig: AdMobFreeInterstitialConfig = {
      isTesting: true, 
      autoShow: true
      
  };

  this.admob.interstitial.config(interstitialConfig);

  this.admob.interstitial.prepare().then(() => {
      // success
  });

}

}
