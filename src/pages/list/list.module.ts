import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListPage } from './list';
import { NgxQRCodeModule } from 'ngx-qrcode2';
import { QrCodeModalComponent } from '../../components/qr-code-modal/qr-code-modal';
import { NewListModalComponent } from '../../components/new-list-modal/new-list-modal';
import { EditListModalComponent } from '../../components/edit-list-modal/edit-list-modal';

@NgModule({
  declarations: [
    ListPage,
    QrCodeModalComponent,
    NewListModalComponent,
    EditListModalComponent
  ],
  entryComponents: [
    QrCodeModalComponent,
    NewListModalComponent,
    EditListModalComponent],
  imports: [
    NgxQRCodeModule,
    IonicPageModule.forChild(ListPage)
  ],
})
export class ListPageModule {}
