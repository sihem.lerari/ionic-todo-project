import { Component } from '@angular/core';
import { NavController,  ModalController} from 'ionic-angular';
import { AuthProvider } from '../../providers/auth';
import { AlertProvider } from '../../providers/alert';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { ToastProvider } from '../../providers/toast';
import { ItemListPage } from '../../pages/item-list/item-list';
import { QrCodeModalComponent } from '../../components/qr-code-modal/qr-code-modal';
import { NewListModalComponent } from '../../components/new-list-modal/new-list-modal';
import { EditListModalComponent } from '../../components/edit-list-modal/edit-list-modal';
import { Observable } from "rxjs/Observable";
import { BarcodeScanner } from '@ionic-native/barcode-scanner';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  todoList: Observable<any[]>;
  segment = 'myTodoList';
  userId:string;
  scannedCode: string;



  constructor(public navCtrl: NavController,
              public modalCtrl: ModalController,
              private todoFbProvider: TodoFirebaseProvider,
              private alertProvider: AlertProvider,
              private authProvider:AuthProvider,
              private toastCtrl: ToastProvider,
              private barcodeScanner: BarcodeScanner){
  }

  ionViewWillLoad(){
    this.authProvider.getAuthState().subscribe(userData =>{ console.log(userData)
      this.userId = userData.uid;
      this.toastCtrl.showToast(
        `Bienvenue ${userData.displayName}`,
        3000,
        'bottom'
      )
    })
  }

  ionViewDidLoad() {
    this.todoList = this.todoFbProvider.getLists();
  }

  goToItems(listId: string){
      this.navCtrl.push(ItemListPage,{ listId: listId });
  }

  numberOfItemsInList(items): number {
  if ( items !== undefined) {
    return Object.keys(items).map(i => items[i]).length;
  }
  return 0;
}


  addTodoList() {
    let modal = this.modalCtrl.create(NewListModalComponent);
    modal.present();
  }


 updateTodoList(list) {
   let modal = this.modalCtrl.create(EditListModalComponent, { list :list});
   modal.present();
}

 deleteTodoList(item){
   this.alertProvider.confirmAlert({
     title: "Attention!",
     message:"Are you sure you want to delete this list",
     button_1: {text: "No", handler : () => {console.log('No clicked')} },
     button_2: {text: "Yes", handler : () => {
       this.todoFbProvider.deleteList(item);
     } }
   }).present();
  }


  shareTodoListWithQrCode(list){
    let modal = this.modalCtrl.create(QrCodeModalComponent, {listId :list.uuid});
    modal.present();
  }

  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = barcodeData.text;
      this.todoFbProvider.partageList(this.scannedCode);
    }, (err) => {
        console.log('Error: ', err);
    });
  }

}
