import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewItemPage } from './new-item';
import { MapModalComponent } from '../../components/map-modal/map-modal';
import { DesciptionItemModalComponent } from '../../components/desciption-item-modal/desciption-item-modal';

@NgModule({
  declarations: [
    NewItemPage,
    MapModalComponent,
    DesciptionItemModalComponent
  ],
  entryComponents: [
    MapModalComponent,
    DesciptionItemModalComponent],
  imports: [
    IonicPageModule.forChild(NewItemPage),
  ],
})
export class NewItemPageModule {}
