import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { TodoItem } from '../../interfaces/todo';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { ImageUploaderProvider } from "../../providers/image-uploader";
import { MapModalComponent } from '../../components/map-modal/map-modal';
import { DesciptionItemModalComponent } from '../../components/desciption-item-modal/desciption-item-modal';



@IonicPage()
@Component({
  selector: 'page-new-item',
  templateUrl: 'new-item.html',
})
export class NewItemPage {

  addedItem: TodoItem;

  constructor(public navParams: NavParams,
              public modalCtrl: ModalController,
              public navCtrl: NavController,
              public viewCtrl: ViewController,
              private todoFbProvider: TodoFirebaseProvider,
              private imgUploaderProvider: ImageUploaderProvider) {
    this.addedItem={} as TodoItem;
    this.addedItem.image = "assets/imgs/img-default.png"
  }

  addItem(){
    let listId= this.navParams.get('listId');
    this.addedItem.complete = false;
    this.todoFbProvider.addItem(listId, this.addedItem);
    this.viewCtrl.dismiss();

}
  selectImage(){
  this.imgUploaderProvider.selectImage().then(res=> this.addedItem.image=res );
  }

  locationItem(){
    this.navCtrl.push(MapModalComponent, {position: null, showBtn:true});
  }
  public ionViewWillEnter() {
    // retirieve data from map
    let pos = this.navParams.get('locationOfItem')|| null;
      if (pos !== null ){
          this.addedItem.latitude= pos.lat;
          this.addedItem.longitude= pos.lng;
      }
  }

  descriptionOfItem(){
    let modal = this.modalCtrl.create(DesciptionItemModalComponent, {desciption: ''});
    modal.onDidDismiss(desciption => {
     this.addedItem.desc= desciption
    });
    modal.present();

  }



}
