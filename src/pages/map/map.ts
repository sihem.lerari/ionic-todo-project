import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { GoogleMapsProvider } from '../../providers/google-maps';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';



@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {


  allItems:any[];
  constructor(public navCtrl: NavController,
              public platform: Platform,
              private googleMapsProvider: GoogleMapsProvider,
              private todoFbProvider: TodoFirebaseProvider
            ) {

    platform.ready().then(() => {
      this.todoFbProvider.getLists().subscribe((lists) =>
      {
        for (let l of lists) {
            console.log('list', l)
            console.log('l.items',l.items);
            this.allItems= this.allItems.concat(l.items);    // TODO: Not working at this line !!!
        }
          console.log('all', this.allItems);
          this.googleMapsProvider.loadMapWithAllMarkers('map', this.allItems);
      })
      })
}


}
