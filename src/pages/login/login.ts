import { AuthProvider } from '../../providers/auth';
//import { TabsPage} from '../../pages/tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';
import { SLIDES } from '../../config/slides.data';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})


export class LoginPage {

  slides= SLIDES;

  constructor(public navCtrl: NavController,
              private authService :AuthProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }


  loginWithGoogle(){
    this.authService.loginWithGoogle()
    // .then(user => {
    //      console.log("hi")
    //      //this.navCtrl.setRoot(TabsPage);
    //  })
    // .catch(err => console.log(err.message))
}

  loginWithFacebook(){
    this.authService.loginWithFacebook();
   /* this.navCtrl.setRoot(TabsPage); */
  }
}
