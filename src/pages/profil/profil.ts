import { Component } from '@angular/core';
import {AuthProvider} from "../../providers/auth";

@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {

  name:string;
  picture:string;


  constructor(private authProvider:AuthProvider) {
    this.authProvider.getAuthState().subscribe(userData =>{
      this.name = userData.displayName;
      this.picture = userData.photoURL;
    })
  }

  logOut(){
    this.authProvider.logout();
  }

}
