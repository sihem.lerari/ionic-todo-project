import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { TodoItem } from "../../interfaces/todo";
import { AlertProvider } from '../../providers/alert';
import { TodoFirebaseProvider } from '../../providers/todo-firebase';
import { EditItemPage } from '../../pages/edit-item/edit-item';
import { MapModalComponent } from '../../components/map-modal/map-modal';


@IonicPage()
@Component({
  selector: 'page-item',
  templateUrl: 'item.html',
})
export class ItemPage {

  item: TodoItem;
  listUuid: string;
  complete:string;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertProvider: AlertProvider,
              private todoFbProvider: TodoFirebaseProvider,
              public platform: Platform,
              public actionsheetCtrl: ActionSheetController) {

        this.item =this.navParams.get('item');
        this.listUuid =this.navParams.get('listId');

  }

  openMenu() {
  let actionSheet = this.actionsheetCtrl.create({
    title: '',
    cssClass: 'action-sheets-basic-page',
    buttons: [
      {
        text: 'Complete',
        icon: !this.platform.is('ios') ? 'done-all' : null,
        handler: () => {
          this.completeItem();
        }
      },
      {
        text: 'Modifier',
        icon: !this.platform.is('ios') ? 'create' : null,
        handler: () => {
          this.updateTodoItem();
        }
      },
      {
        text: 'Supprimer',
        role: 'destructive',
        icon: !this.platform.is('ios') ? 'trash' : null,
        handler: () => {
          this.deleteTodoItem();
        }
      },
      {
        text: 'Annuler',
        role: 'cancel', // will always sort to be on the bottom
        icon: !this.platform.is('ios') ? 'close' : null,
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  actionSheet.present();
}

  private completeItem(){
    this.item.complete= true;
    this.todoFbProvider.updateItem(this.listUuid, this.item);
  }

  private updateTodoItem() {
      this.navCtrl.push(EditItemPage,{listId: this.listUuid, item: this.item});
  }

  private deleteTodoItem() {
    this.alertProvider.confirmAlert({
      title: "Attention!",
      message: "Are you sure you want to delete this item",
      button_1: {
        text: "No", handler: () => {
          console.log('No clicked')
        }
      },
      button_2: {
        text: "Yes", handler: () => {
          this.todoFbProvider.deleteItem(this.listUuid, this.item.uuid).then(() => {
            this.navCtrl.pop();
          }
        );
        }
      }
    }).present();
  }

  locationItem(){
    this.navCtrl.push(MapModalComponent, {position: {latitude : this.item.latitude, longitude: this.item.longitude}, showBtn:false});
  }


}
