import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemPage } from './item';
import { EditItemPage } from '../edit-item/edit-item';

@NgModule({
  declarations: [
    ItemPage,
    EditItemPage
  ],
  entryComponents: [
    EditItemPage ],
  imports: [
    IonicPageModule.forChild(ItemPage),
  ],
})
export class ItemPageModule {}
