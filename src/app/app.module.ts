import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { FIREBASE_CONFIG } from '../config/firebase.config';

/* Pages */
import { MapPage } from '../pages/map/map';
import { ProfilPage } from '../pages/profil/profil';
import { TabsPage } from '../pages/tabs/tabs';

/* Modules */
import { ListPageModule } from '../pages/list/list.module';
import { LoginPageModule } from '../pages/login/login.module';
import { ItemListPageModule } from '../pages/item-list/item-list.module';
import { ItemPageModule } from '../pages/item/item.module';
//import { NewItemPageModule } from '../pages/new-item/new-item.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/* Provides*/
import { AlertProvider } from '../providers/alert';
import { AuthProvider } from '../providers/auth';
import { TodoFirebaseProvider } from '../providers/todo-firebase';
import { ImageUploaderProvider } from '../providers/image-uploader';
import { GoogleMapsProvider } from '../providers/google-maps';
import { LocalNotificationsProvider } from '../providers/local-notifications';
import { SpeechRecognitionProvider } from '../providers/speech-recognition';
import { ToastProvider } from '../providers/toast';

/* Ionic Native */
import { GooglePlus } from '@ionic-native/google-plus';
import { Facebook } from '@ionic-native/facebook';
import { AdMobFree } from '@ionic-native/admob-free';
import { Geolocation } from '@ionic-native/geolocation';
import { Camera } from '@ionic-native/camera';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GoogleMaps } from '@ionic-native/google-maps';
import { SpeechRecognition } from '@ionic-native/speech-recognition';
import { LocalNotifications } from '@ionic-native/local-notifications';


@NgModule({
  declarations: [
    MyApp,
    MapPage,
    ProfilPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    ListPageModule,
    ItemListPageModule,
    LoginPageModule,
    ItemPageModule,
  //  NewItemPageModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule,
    AngularFireDatabaseModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MapPage,
    ProfilPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AlertProvider,
    AuthProvider,
    GooglePlus,
    Facebook,
    AdMobFree,
    Geolocation,
    Camera,
    TodoFirebaseProvider,
    ImageUploaderProvider,
    BarcodeScanner,
    GoogleMaps,
    SpeechRecognition,
    LocalNotifications,
    GoogleMapsProvider,
    LocalNotificationsProvider,
    SpeechRecognitionProvider,
    ToastProvider
  ]
})
export class AppModule {}
